#*****************************#
# Class svm: implements a support vector machine for classification purpose;
# Algorithm: ERM of the Hinge loss function by gradient descend;
# Space of inputs: R^d (eventually declare a feature map or a kernel);
# Space of outputs: {-1,1};
# It can be used both in linear case and in non linear case (by defining an external feature map or kernel);
# Inheritance: None;
# Author: Derin Lucio;
#*****************************#

#*******CONSTRUCTOR:********#
# @n: int, required, dimension of the space of inputs (i.e. the number of features);
# @verbose: optional, bool to enable verbose output (such as %process while training), default: false;
# @algType: optional, string to specify the type of algorithm; possible values: "linear", "feature", "kernel"; default value: "linear"
# @featureMap: optional, required only if algType is "feature" or "kernel"; it's the external feature map; default: None;
# def __init__(self, n, verbose=False, algType="linear", featureMap=None);

#**********SETTERS:**********#
# setAlgorithmType(algType) -> Set algorithm type; type can be: "linear", "feature", "kernel";
# enableVerbosePrinting() -> Enable verbose printing;
# disableVerbosePrinting() -> Disable verbose printing;
# setInputSpaceDim(n) -> Set input space dimension (number of features) equal to @n;
# setSeed(seed) -> Set seed for the random initialization of wt;
# setFeatureMap(map) -> Set feature map (callable object);

#**********GETTERS:**********#
# getInputSpaceDim() -> Get the dimension of the input space (number of features);
# getWt() -> get the current wt;

#**********METHODS:**********#
# Train the svm:
# @x: matrix of data (i-th row of x contains the coordinates of the i-th training point);
# @y: vector of outputs (i-th element of y contains the classification (-1 or 1) of the i-th training point);
# @tStop: number of iterations in Gradient Descend algorithm;
# @lamb: regularization parameter;
# wt is initialized as a vector of random number;
# The training process implements the gradient descend and finds Wt_hat, the minimizer of the empirical risk;
# At the end of the training process, m_wt (private member of the class) is the best minimizer found;
# train(x, y, tStop, lamb);

# Get the classification of a point:
# @x: vector of the input space that is to be classified;
# return: classification of @x based on the best minimizer of ER found in training;
# return type: int {-1,1};
# int classify(x);

# Notoation:
# ERM = empirical risk minimization;
# ER = empirical risk;
# GD = Gradient Descend;
# wt = vector updated in the gradient descend;
# wt_hat = best minimizer of ER found with GD;
# feature map = function that maps input parameters in a vector space on which the SVM can work;

# feature map = function that maps input parameters in a vector space on which the SVM can work;
# fMap: R^d -> R^d; x |-> fMap(x);

# kernel map: kernel evaluation of the regression in the point x;
# kerMap: R^d x R^d -> R; x |-> sum(K(x,xTrainingj),wtj);
# where:
# x is the point of evaluation of the regression;
# xTrainingj is the j-th element of the training set;
# wtj is the j-th element of the vector on which the GD algorithm is acting;
#****************************#

import numpy as np
import random as r
import time
import math as m


# Finds the vector in x with the maximum norm
# and returns it's squared norm
# @x: vector of vectors
# it's used in the GD's step evaluation


def maxSquaredNorm(x):
    maximum = 0.
    conf = 0.
    for xx in x[0]:
        maximum += xx**2
    for element in x:
        for xx in element:
            conf += xx**2
        if conf > maximum:
            maximum = conf
        conf = 0.
    return maximum

# subgradient of Hinge Loss function;
# let z = y*(t(w)*x) be the product of the expected output and the real output;
# if z<=1, the value of the subgradient of the Hinge loss function is -1;
# if z>1, the value of the subgradient of the Hinge loss function is 0;
# @y: label of the training point x;
# @wt: vector on which the GD is acting;
# @x: training point;
# return: int, {-1,0};


def dl(y, w, x):
    if(y*np.dot(w, x) <= 1):
        return -1
    else:
        return 0

# vector in the subgradient of ER;
# @y: vector of outputs (i-th element of y contains the classification (-1 or 1) of the i-th training point);
# @x: matrix of data (i-th row of x contains the coordinates of the i-th training point);
# @lamb: regularization parameter;
# @wt: vector on which the GD is acting;


def vt(y, x, lamb, wt):
    vt = np.zeros(shape=(2))
    for i in range(0, x.shape[0]):
        vt = vt + (y[i]*x[i]*dl(y[i], wt, x[i]))
    vt = (x.shape[0]**-1)*vt
    vt = vt+(2*lamb*wt)
    return vt

# vector in the subgradient of the E.R. in kernel formulation;
# @y: vector of outputs (i-th element of y contains the classification (-1 or 1) of the i-th training point);
# @x: matrix of data (i-th row of x contains the coordinates of the i-th training point);
# @wt: vector on which the GD is acting;
# @k: kernel map;


def S(y, x, wt, k):
    xSize = x.shape[0]
    s = np.zeros(shape=xSize)
    for i in range(0, xSize):
        if(y[i]*k(x[i], wt) <= 1):
            s[i] = -y[i]
            s[i] = s[i]/xSize
    return s

# svm class


class svm():
    wt = np.array([])
    seed = 0
    ##########################
    #######CONSTRUCTOR:#######
    ##########################

    def __init__(self, n, verbose=False, algType="linear", featureMap=None):
        self.n = n
        self.verbose = verbose
        self.algType = algType
        self.featureMap = featureMap
        self.wt.resize(n)

    ##########################
    ########SETTERS:##########
    ##########################

    def setAlgorithmType(self, algType):
        self.algType = algType

    def enableVerbose(self):
        self.verbose = True

    def disableVerbose(self):
        self.verbose = False

    def setInputSpaceDim(self, n):
        self.n = n
        self.wt = np.empty(shape=n)

    def setSeed(self, seed):
        self.seed = seed

    def setFeatureMap(self, fmap):
        self.featureMap = fmap

    ##########################
    ########GETTERS:##########
    ##########################

    def getInputSpaceDim(self):
        return self.n

    def getWt(self):
        return self.wt

    ##########################
    ########METHODS:##########
    ##########################

    # train method;
    # Finds the minimizator of ER using GD;
    # @x: matrix of data (i-th row of x contains the coordinates of the i-th training point);
    # @y: vector of outputs (i-th element of y contains the classification (-1 or 1) of the i-th training point);
    # @tStop: number of iterations in Gradient Descend algorithm;
    # @lamb: regularization parameter;
    def train(self, x, y, tStop, lamb):

        if(x.shape[0] != y.shape[0]):
            print("Error in SVM train: x and y must have the same size;")
            return

        if self.algType != "kernel":
            # random initialization of wt
            # if the seed hasn't been set, set seed
            if(self.seed == 0):
                r.seed(time.time())

            for i in range(0, self.n):
                self.wt[i] = r.random()

            # if in feature case, first evaluate the mapped training points and then minimize ER
            if self.algType == "feature":
                # mapped training points
                xFeature = np.zeros(shape=(x.shape[0], self.n))
                for i in range(0, xFeature.shape[0]):
                    xFeature[i] = self.featureMap(x[i])
                B = maxSquaredNorm(xFeature) + 2*lamb
                B = 1/B
                # GD
                for t in range(1, tStop+1):
                    self.wt = self.wt - ((B/m.sqrt(t))*vt(y, x, lamb, self.wt))
                    if(self.verbose and t % 10 == 0):
                        print("training...", round(
                            float(t/tStop)*100), "%", end="\r")
            # if in linear case, minimize the ER
            elif self.algType == "linear":
                B = maxSquaredNorm(x) + 2*lamb
                B = 1/B
                # GD
                for t in range(1, tStop+1):
                    self.wt = self.wt - ((B/m.sqrt(t))*vt(y, x, lamb, self.wt))
                    if(self.verbose and t % 10 == 0):
                        print("training...", round(
                            float(t/tStop)*100), "%", end="\r")

        elif self.algType == "kernel":
            xSize = x.shape[0]
            self.wt = np.empty(shape=xSize)
            # random initialization of wt
            # if the seed hasn't been set, set seed
            if(self.seed == 0):
                r.seed(time.time())

            for i in range(0, xSize):
                self.wt[i] = r.random()
            B = maxSquaredNorm(x) + 2*lamb
            B = 1/B
            # GD
            for t in range(1, tStop+1):
                c = S(y, x, self.wt, self.featureMap)
                c = c + 2*lamb*self.wt
                c = (B/m.sqrt(t))*c
                self.wt = self.wt - c
                if(self.verbose and t % 10 == 0):
                    print("training...", round(
                        float(t/tStop)*100), "%", end="\r")

        else:
            print("Unknown error in train.")

    # Get the classification of a point:
    # @x: vector of the input space that is to be classified;
    # return: classification of @x based on the best minimizer of ER found in training;
    # return type: int {-1,1};
    def classify(self, x):
        # if feature map, first evaluate the mapped point and then classify
        if self.algType != "kernel":
            if self.algType == "feature":
                return np.sign(np.dot(self.wt, self.featureMap(x)))
            # if linear case, classify the point
            if self.algType == "linear":
                return np.sign(np.dot(self.wt, x))
        elif self.algType == "kernel":
            return np.sign(self.featureMap(x, self.wt))
        else:
            print("Unknown error in classify.")

    # Get the regression of a point:
    # @x: vector of the input space that is to be classified;
    # return: classification of @x based on the best minimizer of ER found in training;
    # return type: double;
    def regression(self, x):
        # if feature map, first evaluate the mapped point and then classify
        if self.algType != "kernel":
            if self.algType == "feature":
                return np.dot(self.wt, self.featureMap(x))
            # if linear case, classify the point
            if self.algType == "linear":
                return np.dot(self.wt, x)
        elif self.algType == "kernel":
            return self.featureMap(x, self.wt)
        else:
            print("Unknown error in classify.")
