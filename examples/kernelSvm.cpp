//Author: Derin Lucio
//Simple example of a kernel SVM training and validation
//c++
#include <iostream>
#include <time.h>
#include <vector>
//ROOT
#include <TApplication.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TH2D.h>
#include <TRandom3.h>
//svm class
#include "../svm.h"

namespace globalData {
std::vector<std::vector<double>> x;
double sigma = 0.1;
} // namespace globalData

double gaussianKernel(const std::vector<double> &xi, const std::vector<double> &xj, const double sigma) {
    if (xi.size() != xj.size()) {
        std::cerr << "Error in gaussianKernel: can't evaluate the distance between vectors of different sizes.\n";
        return 0.;
    }
    //evaluating squared distances
    double dist = 0;
    for (int i = 0; i < xi.size(); i++) {
        dist += std::pow(xi[i] - xj[i], 2);
    }
    //return Kij
    return std::exp(-(dist) / (2 * std::pow(sigma, 2)));
}

double kernelMap(const std::vector<double> &x, const std::vector<double> wt) {
    if (globalData::x.size() != wt.size()) {
        std::cerr << "Error in kernelMap: training set and wt are of differernt dimensions.\n";
        return 0.;
    }

    std::vector<double> kxj(wt.size(), 0.);
    for (int i = 0; i < wt.size(); i++)
        kxj[i] = gaussianKernel(x, globalData::x[i], globalData::sigma);
    double f = 0.;
    for (int i = 0; i < wt.size(); i++)
        f += kxj[i] * wt[i];
    return f;
}

int main() {
    TApplication app("app", NULL, NULL);
    TRandom3 r;
    r.SetSeed(time(NULL));

    //training points
    int nSignal = 100, nNoise = 100;
    int xDim = nSignal + nNoise;
    globalData::x.resize(nSignal + nNoise);
    std::vector<int> y(nSignal + nNoise);
    //generating signal
    for (int i = 0; i < nSignal; i++) {
        globalData::x[i] = {r.Gaus(0, 0.5), r.Gaus(2, 0.5)};
        y[i] = 1;
    }
    //generating noise
    for (int i = 0; i < nNoise; i++) {
        double xAxis = (r.Rndm() * 4) - 2;
        globalData::x[i + nSignal] = {xAxis, std::pow(xAxis, 2) + r.Gaus(0, 0.1)};
        y[i + nSignal] = -1;
    }
    //graphs for training set plot
    TGraph trainingSignal, trainingNoise;
    for (int i = 0; i < nSignal; i++)
        trainingSignal.SetPoint(i, globalData::x[i][0], globalData::x[i][1]);
    for (int i = nSignal; i < nSignal + nNoise; i++)
        trainingNoise.SetPoint(i - nSignal, globalData::x[i][0], globalData::x[i][1]);

    //training svm
    //sigma of the gaussian kernel
    svm mySvm(2, "kernel", kernelMap, true);

    int tStop = 10000;
    double lamb = 1;
    mySvm.setSeed(1);
    mySvm.train(globalData::x, y, tStop, lamb);

    //validation set
    nSignal = 10000;
    nNoise = 10000;
    TGraph signal, noise;
    std::vector<std::vector<double>> xValidation(nSignal + nNoise);
    std::vector<int> yValidation(nSignal + nNoise);
    int errSignal = 0, errNoise = 0;

    //generating signal
    for (int i = 0; i < nSignal; i++) {
        xValidation[i] = {r.Gaus(0, 0.5), r.Gaus(2, 0.5)};
    }
    //generating noise
    for (int i = 0; i < nNoise; i++) {
        double xAxis = (r.Rndm() * 4) - 2;
        xValidation[i + nSignal] = {xAxis, std::pow(xAxis, 2) + r.Gaus(0, 0.1)};
    }
    //Filling graphs
    for (int i = 0; i < nSignal; i++) {
        signal.SetPoint(i, xValidation[i][0], xValidation[i][1]);
        if (mySvm.classify({xValidation[i][0], xValidation[i][1]}) != 1)
            errSignal++;
    }
    for (int i = nSignal; i < (nSignal + nNoise); i++) {
        noise.SetPoint(i - nSignal, xValidation[i][0], xValidation[i][1]);
        if (mySvm.classify({xValidation[i][0], xValidation[i][1]}) != -1)
            errNoise++;
    }
    std::cout << "Validation Error Ratio (error/population): " << (double)(errSignal + errNoise) / (double)(xValidation.size()) << ";\n";
    std::cout << "False Positive Ratio (f.p./population): " << (double)(errNoise) / (double)(xValidation.size()) << ";\n";
    std::cout << "False Negative Ratio (f.n./population): " << (double)(errSignal) / (double)(xValidation.size()) << ";\n";

    signal.SetMarkerStyle(20);
    signal.SetMarkerSize(0.5);
    signal.SetMarkerColor(kGreen);
    noise.SetMarkerStyle(20);
    noise.SetMarkerSize(0.5);
    noise.SetMarkerColor(kRed);

    //Drawing contour
    std::vector<double> xAxis, yAxis;
    double step = 0.01, xmin = -2, xmax = 2, ymin = 0, ymax = 4;
    double xAxisSize = (xmax - xmin) / step;
    double yAxisSize = (ymax - ymin) / step;
    for (int i = 0; i < xAxisSize; i++) {
        xAxis.push_back(xmin + (step * i));
    }
    for (int i = 0; i < yAxisSize; i++) {
        yAxis.push_back(ymin + (step * i));
    }

    TGraph2D validation;
    for (int i = 0; i < xAxis.size(); i++) {
        for (int j = 0; j < yAxis.size(); j++)
            validation.SetPoint((i * xAxis.size()) + j, xAxis[i], yAxis[j], (double)mySvm.classify({xAxis[i], yAxis[j]}));
    }

    auto contourLevel = validation.GetContourList(0);
    TCanvas c1;
    c1.DrawFrame(xmin, ymin, xmax, ymax);
    signal.Draw("P");
    noise.Draw("P");
    contourLevel[0].Draw("SAME");

    TCanvas c2;
    c2.DrawFrame(xmin, ymin, xmax, ymax);
    trainingSignal.SetMarkerStyle(20);
    trainingSignal.SetMarkerSize(0.5);
    trainingSignal.SetMarkerColor(kGreen);
    trainingNoise.SetMarkerStyle(20);
    trainingNoise.SetMarkerSize(0.5);
    trainingNoise.SetMarkerColor(kRed);

    trainingSignal.Draw("P");
    trainingNoise.Draw("P");
    contourLevel[0].Draw("SAME");

    app.Run(true);
    return 0;
}
