//Author: Derin Lucio
//Simple example of a feature SVM training and validation
//c++
#include <iostream>
#include <time.h>
#include <vector>
//ROOT
#include <TApplication.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TH2D.h>
#include <TRandom3.h>
//svm class
#include "../svm.h"

std::vector<double> featureMap(const std::vector<double> &x) {
    return {std::pow(x[0], 2), x[1] - 0.3};
}

int main() {
    TApplication app("app", NULL, NULL);
    svm mySvm(2, "feature", featureMap, true);
    TRandom3 r;
    r.SetSeed(time(NULL));

    //training points
    int nSignal = 10000, nNoise = 10000;

    std::vector<std::vector<double>> x(nSignal + nNoise);
    std::vector<int> y(nSignal + nNoise);
    //generating signal
    for (int i = 0; i < nSignal; i++) {
        x[i] = {r.Gaus(0, 0.5), r.Gaus(2, 0.5)};
        y[i] = 1;
    }
    //generating noise
    for (int i = 0; i < nNoise; i++) {
        double xAxis = r.Gaus(0, 2 / 3.);
        x[i + nSignal] = {xAxis, std::pow(xAxis, 2) + r.Gaus(0, 0.1)};
        y[i + nSignal] = -1;
    }

    //training svm
    int tStop = 10000;
    double lamb = .08;
    mySvm.setSeed(1);
    mySvm.train(x, y, tStop, lamb);

    //validation set
    TGraph signal, noise;
    std::vector<std::vector<double>> xValidation(nSignal + nNoise);
    std::vector<int> yValidation(nSignal + nNoise);
    int errSignal = 0, errNoise = 0;

    //generating signal
    for (int i = 0; i < nSignal; i++) {
        xValidation[i] = {r.Gaus(0, 0.5), r.Gaus(2, 0.5)};
    }
    //generating noise
    for (int i = 0; i < nNoise; i++) {
        double xAxis = r.Gaus(0, 2 / 3.);
        xValidation[i + nSignal] = {xAxis, std::pow(xAxis, 2) + r.Gaus(0, 0.1)};
    }
    //Filling graphs
    for (int i = 0; i < nSignal; i++) {
        signal.SetPoint(i, xValidation[i][0], xValidation[i][1]);
        if (mySvm.classify({xValidation[i][0], xValidation[i][1]}) != 1)
            errSignal++;
    }
    for (int i = nSignal; i < (nSignal + nNoise); i++) {
        noise.SetPoint(i - nSignal, xValidation[i][0], xValidation[i][1]);
        if (mySvm.classify({xValidation[i][0], xValidation[i][1]}) != -1)
            errNoise++;
    }
    std::cout << "Validation Error Ratio (error/population): " << (double)(errSignal + errNoise) / (double)(xValidation.size()) << ";\n";
    std::cout << "False Positive Ratio (f.p./population): " << (double)(errNoise) / (double)(xValidation.size()) << ";\n";
    std::cout << "False Negative Ratio (f.n./population): " << (double)(errSignal) / (double)(xValidation.size()) << ";\n";

    signal.SetMarkerStyle(20);
    signal.SetMarkerSize(0.5);
    signal.SetMarkerColor(kGreen);
    noise.SetMarkerStyle(20);
    noise.SetMarkerSize(0.5);
    noise.SetMarkerColor(kRed);

    //Drawing contour
    std::vector<double> xAxis, yAxis;
    double step = 0.01, xmin = -2, xmax = 2, ymin = 0, ymax = 4;
    double xAxisSize = (xmax - xmin) / step;
    double yAxisSize = (ymax - ymin) / step;
    for (int i = 0; i < xAxisSize; i++) {
        xAxis.push_back(xmin + (step * i));
    }
    for (int i = 0; i < yAxisSize; i++) {
        yAxis.push_back(ymin + (step * i));
    }

    TGraph2D validation;
    for (int i = 0; i < xAxis.size(); i++) {
        for (int j = 0; j < yAxis.size(); j++)
            validation.SetPoint((i * xAxis.size()) + j, xAxis[i], yAxis[j], (double)mySvm.classify({xAxis[i], yAxis[j]}));
    }

    auto contourLevel = validation.GetContourList(0);
    TCanvas c1;
    c1.DrawFrame(xmin, ymin, xmax, ymax);
    signal.Draw("P");
    noise.Draw("P");
    contourLevel[0].Draw("SAME");

    app.Run(true);
    return 0;
}