# Author: Derin Lucio
# Simple example of a feature SVM training and validation
from matplotlib import pyplot as plt
import random as r
from svm import svm
import numpy as np


def fmap(x):
    return np.array([x[0], x[1]-1])


if __name__ == "__main__":

    # generating training set
    nSignal = 1000
    nNoise = 1000
    x = np.zeros(shape=(nSignal+nNoise, 2))
    y = np.ones(shape=(nSignal+nNoise))
    for i in range(0, nSignal):
        x[i] = [r.gauss(-2, 1), r.gauss(3, 1)]
    for i in range(nSignal, nSignal+nNoise):
        x[i] = [r.gauss(2, 1), r.gauss(-1, 1)]
        y[i] = -1

    # train
    mySvm = svm(2, True, 'feature', fmap)
    tStop = 1000
    lamb = 1e-9
    mySvm.train(x, y, tStop, lamb)
    xAxis = np.linspace(-5, 5, 1000)
    yAxis = np.linspace(-5, 5, 1000)
    mat = np.zeros(shape=(1000, 1000))
    for j in range(0, 1000):
        for i in range(0, 1000):
            mat[i][j] = mySvm.classify([xAxis[j], yAxis[i]])

    # plot
    plt.figure(0)
    signalx = [x[i][0] for i in range(0, nSignal)]
    signaly = [x[i][1] for i in range(0, nSignal)]
    plt.scatter(signalx, signaly, s=1, color='green', label='signal')
    noisex = [x[i][0] for i in range(nSignal, nSignal+nNoise)]
    noisey = [x[i][1] for i in range(nSignal, nSignal+nNoise)]
    plt.scatter(noisex, noisey, s=1, color='red', label='noise')
    plt.title('Training Set')
    plt.grid(True)
    plt.contour(xAxis, yAxis, mat, levels=[0])
    plt.show()
