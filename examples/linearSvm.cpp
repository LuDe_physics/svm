//Author: Derin Lucio
//Simple example of a linear SVM training and validation
//c++
#include <iostream>
#include <time.h>
#include <vector>
//ROOT
#include <TApplication.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TH2D.h>
#include <TRandom3.h>

//svm class
#include "../svm.h"

int main() {
    TApplication app("app", NULL, NULL);
    svm mySvm(2);
    TRandom3 r;
    r.SetSeed(time(NULL));

    //training points
    int nSignal = 10000, nNoise = 10000;

    std::vector<std::vector<double>> x(nSignal + nNoise);
    std::vector<int> y(nSignal + nNoise);
    //generating signal
    for (int i = 0; i < nSignal; i++) {
        x[i] = {r.Gaus(-2, 0.8), r.Gaus(2, 0.8)};
        y[i] = 1;
    }
    //generating noise
    for (int i = 0; i < nNoise; i++) {
        double xAxis = (r.Rndm() * 4) - 2;
        x[i + nSignal] = {r.Gaus(2, 0.8), r.Gaus(-2, 0.8)};
        y[i + nSignal] = -1;
    }

    //training svm
    int tStop = 1000;
    double lamb = 1;
    mySvm.train(x, y, tStop, lamb);

    //validation set
    TGraph signal, noise;

    std::vector<std::vector<double>> xValidation(nSignal + nNoise);
    std::vector<int> yValidation(nSignal + nNoise);
    int errSignal = 0, errNoise = 0;

    //generating signal
    for (int i = 0; i < nSignal; i++) {
        xValidation[i] = {r.Gaus(-2, 0.8), r.Gaus(2, 0.8)};
    }
    //generating noise
    for (int i = 0; i < nNoise; i++) {
        xValidation[i + nSignal] = {r.Gaus(2, 0.8), r.Gaus(-2, 0.8)};
    }

    //Filling graphs
    for (int i = 0; i < nSignal; i++) {
        signal.SetPoint(i, xValidation[i][0], xValidation[i][1]);
        if (mySvm.classify({xValidation[i][0], xValidation[i][1]}) != 1)
            errSignal++;
    }
    for (int i = nSignal; i < (nSignal + nNoise); i++) {
        noise.SetPoint(i - nSignal, xValidation[i][0], xValidation[i][1]);
        if (mySvm.classify({xValidation[i][0], xValidation[i][1]}) != -1)
            errNoise++;
    }
    std::cout << "Validation Error Ratio (error/population): " << (double)(errSignal + errNoise) / (double)(xValidation.size()) << ";\n";
    std::cout << "False Positive Ratio (f.p./population): " << (double)(errNoise) / (double)(xValidation.size()) << ";\n";
    std::cout << "False Negative Ratio (f.n./population): " << (double)(errSignal) / (double)(xValidation.size()) << ";\n";

    signal.SetMarkerStyle(20);
    signal.SetMarkerSize(0.5);
    signal.SetMarkerColor(kGreen);
    noise.SetMarkerStyle(20);
    noise.SetMarkerSize(0.5);
    noise.SetMarkerColor(kRed);

    //Drawing contour
    std::vector<double> xAxis, yAxis;
    double step = 0.05;
    for (double i = -10; i <= 10; i += step) {
        xAxis.push_back(i);
        yAxis.push_back(i);
    }

    TGraph2D validation;
    for (int i = 0; i < xAxis.size(); i++) {
        for (int j = 0; j < yAxis.size(); j++)
            validation.SetPoint((i * xAxis.size()) + j, xAxis[i], yAxis[j], (double)mySvm.classify({xAxis[i], yAxis[j]}));
    }

    auto contourLevel = validation.GetContourList(0);
    TCanvas c1;
    c1.DrawFrame(-5, -5, 5, 5);
    signal.Draw("P");
    noise.Draw("P");
    contourLevel[0].Draw("SAME");

    app.Run(true);
    return 0;
}
