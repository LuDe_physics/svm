# Author: Derin Lucio
# Simple example of a kenrel SVM training and validation
from matplotlib import pyplot as plt
import random as r
from svm import svm
import numpy as np
import math as m


def gaussianKernel(a, b):
    return m.exp(-(np.linalg.norm(a-b)**2)/(2*sigma**2))


def fmap(v, wt):
    fv = 0.
    temp = np.zeros(shape=x.shape[0])
    for i in range(0, x.shape[0]):
        temp[i] = gaussianKernel(v, x[i])
    for i in range(0, x.shape[0]):
        fv += temp[i]*wt[i]
    return fv


if __name__ == "__main__":

    # generating training set
    nSignal = 100
    nNoise = 100
    x = np.zeros(shape=(nSignal+nNoise, 2))
    y = np.ones(shape=(nSignal+nNoise))
    for i in range(0, nSignal):
        x[i] = [r.gauss(-2, 1), r.gauss(3, 1)]
    for i in range(nSignal, nSignal+nNoise):
        x[i] = [r.gauss(2, 1), r.gauss(-1, 1)]
        y[i] = -1

    # train
    mySvm = svm(2, True, 'kernel', fmap)
    tStop = 10000
    lamb = 10
    sigma = 0.1
    mySvm.train(x, y, tStop, lamb)
    grid = 50
    xAxis = np.linspace(-5, 5, grid)
    yAxis = np.linspace(-5, 5, grid)
    mat = np.zeros(shape=(grid, grid))
    for j in range(0, grid):
        for i in range(0, grid):
            mat[i][j] = mySvm.classify([xAxis[j], yAxis[i]])

    # plot
    plt.figure(0)
    signalx = [x[i][0] for i in range(0, nSignal)]
    signaly = [x[i][1] for i in range(0, nSignal)]
    plt.scatter(signalx, signaly, s=1, color='green', label='signal')
    noisex = [x[i][0] for i in range(nSignal, nSignal+nNoise)]
    noisey = [x[i][1] for i in range(nSignal, nSignal+nNoise)]
    plt.scatter(noisex, noisey, s=1, color='red', label='noise')
    plt.title('Training Set')
    plt.grid(True)
    plt.contour(xAxis, yAxis, mat, levels=[0])
    plt.show()
