//Author: Derin Lucio
//Esemplification of the effects of early stopping and regularization (a.k.a. penalization)
//acting on a noisy training set to reduce the overfitting effect.
//The training set is the same as kernelSvmNoised example.
//This example only aims to show the qualitative behaviour of the error in relation to
//the number of training iterations or to the value of the regularization parameter lambda.
//In order to finely tune the parameter of an SVM, a more complex KCV pipeline needs to be implemented.

//c++
#include <iostream>
#include <time.h>
#include <vector>
//ROOT
#include <TApplication.h>
#include <TCanvas.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TH2D.h>
#include <TRandom3.h>
//svm class
#include "../svm.h"

namespace globalData {
std::vector<std::vector<double>> x;
double sigma = 0.1;
} // namespace globalData

double gaussianKernel(const std::vector<double> &xi, const std::vector<double> &xj, const double sigma) {
    if (xi.size() != xj.size()) {
        std::cerr << "Error in gaussianKernel: can't evaluate the distance between vectors of different sizes.\n";
        return 0.;
    }
    //evaluating squared distances
    double dist = 0;
    for (int i = 0; i < xi.size(); i++) {
        dist += std::pow(xi[i] - xj[i], 2);
    }
    //return Kij
    return std::exp(-(dist) / (2 * std::pow(sigma, 2)));
}

double kernelMap(const std::vector<double> &x, const std::vector<double> wt) {
    if (globalData::x.size() != wt.size()) {
        std::cerr << "Error in kernelMap: training set and wt are of differernt dimensions.\n";
        return 0.;
    }

    std::vector<double> kxj(wt.size(), 0.);
    for (int i = 0; i < wt.size(); i++)
        kxj[i] = gaussianKernel(x, globalData::x[i], globalData::sigma);
    double f = 0.;
    for (int i = 0; i < wt.size(); i++)
        f += kxj[i] * wt[i];
    return f;
}

int main() {
    TApplication app("app", NULL, NULL);
    TRandom3 r;
    r.SetSeed(time(NULL));

    //training points
    double percentualNoise = 0.1;
    int nSignal = 1000, nNoise = 1000;
    int xDim = nSignal + nNoise;
    globalData::x.resize(nSignal + nNoise);
    std::vector<int> y(nSignal + nNoise);
    //generating signal
    for (int i = 0; i < nSignal; i++) {
        globalData::x[i] = {r.Gaus(0, 0.5), r.Gaus(2, 0.5)};
        y[i] = 1;
        if (r.Rndm() <= percentualNoise)
            y[i] = -1;
    }
    //generating noise
    for (int i = 0; i < nNoise; i++) {
        double xAxis = (r.Rndm() * 4) - 2;
        globalData::x[i + nSignal] = {xAxis, std::pow(xAxis, 2) + r.Gaus(0, 0.1)};
        y[i + nSignal] = -1;
        if (r.Rndm() <= percentualNoise)
            y[i + nSignal] = 1;
    }

    //training svm
    //sigma of the gaussian kernel
    svm mySvm(2, "kernel", kernelMap, true);
    //Early stopping to reject noise
    std::vector<int> tStop = {10, 36, 135, 501};
    std::vector<double> lambda = {0.1, 1, 10, 100};
    mySvm.setSeed(1);

    //validation set
    nSignal = 10000;
    nNoise = 10000;
    std::vector<std::vector<double>> xValidation(nSignal + nNoise);
    std::vector<int> yValidation(nSignal + nNoise);
    std::vector<double> pErrLambda, pErrTStop;
    int error = 0;

    //generating signal
    for (int i = 0; i < nSignal; i++) {
        xValidation[i] = {r.Gaus(0, 0.5), r.Gaus(2, 0.5)};
        yValidation[i] = 1;
    }
    //generating noise
    for (int i = 0; i < nNoise; i++) {
        double xAxis = (r.Rndm() * 4) - 2;
        xValidation[i + nSignal] = {xAxis, std::pow(xAxis, 2) + r.Gaus(0, 0.1)};
        yValidation[i + nSignal] = -1;
    }

    for (auto t : tStop) {
        mySvm.train(globalData::x, y, t, lambda[1]);
        for (int i = 0; i < yValidation.size(); i++)
            if (yValidation[i] * mySvm.classify(xValidation[i]) < 0)
                error++;
        pErrTStop.push_back((double)error / (double)yValidation.size());
        error = 0;
    }

    for (auto l : lambda) {
        mySvm.train(globalData::x, y, tStop[1], l);
        for (int i = 0; i < yValidation.size(); i++)
            if (yValidation[i] * mySvm.classify(xValidation[i]) < 0)
                error++;
        pErrLambda.push_back((double)error / (double)yValidation.size());
        error = 0;
    }

    //graphical output
    TCanvas c1;
    c1.Divide(2);
    c1.cd(1);
    TGraph tStopGr;
    for (int i = 0; i < tStop.size(); i++)
        tStopGr.SetPoint(i, tStop[i], pErrTStop[i]);
    tStopGr.SetMarkerStyle(20);
    tStopGr.SetMarkerSize(0.7);
    tStopGr.SetMarkerColor(kGreen);
    tStopGr.SetTitle("Early stopping effect (fixed #lambda = 1)");
    tStopGr.GetXaxis()->SetTitle("tStop");
    tStopGr.GetYaxis()->SetTitle("Percentual Error (misclassified labels/total labels)");
    c1.GetPad(1)->SetLogx();
    c1.GetPad(1)->SetLogy();
    tStopGr.Draw("APL");

    c1.cd(2);
    TGraph lambdaGr;
    for (int i = 0; i < lambda.size(); i++)
        lambdaGr.SetPoint(i, lambda[i], pErrLambda[i]);
    lambdaGr.SetMarkerStyle(20);
    lambdaGr.SetMarkerSize(0.7);
    lambdaGr.SetMarkerColor(kGreen);
    lambdaGr.SetTitle("Regularization parameter effect (fixed tStop = 36)");
    lambdaGr.GetXaxis()->SetTitle("#lambda");
    lambdaGr.GetYaxis()->SetTitle("Percentual Error (misclassified labels/total labels)");
    c1.GetPad(2)->SetLogx();
    lambdaGr.Draw("APL");

    app.Run(true);
    return 0;
}