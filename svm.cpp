//Author: Derin Lucio;
//Header:
#include "svm.h"
//c++
#include <cmath>
#include <iostream>
#include <random>
#include <time.h>

//Finds the vector in x with the maximum norm
//and returns it's squared norm
//@x: vector of vectors
//it's used in the GD's step evaluation
double maxSquaredNorm(const std::vector<std::vector<double>> &x) {
    int j = x[0].size(), xSize = x.size();
    double max = 0., conf = 0.;
    for (const auto &d : x[0])
        max += std::pow(d, 2);
    for (int i = 0; i < xSize; i++) {
        for (const auto &d : x[i])
            conf += std::pow(d, 2);
        if (conf > max)
            max = conf;
        conf = 0.;
    }
    return max;
}

//vectorial sum;
//@a: first vector, will be modified;
//@b: second vector, const;
//at the end of the execution, a will contain the vectorial sum a+b;
void sumVectorsByComponents(std::vector<double> &a, const std::vector<double> &b) {
    if (a.size() != b.size()) {
        std::cerr << "Error in sumVectorByComponents: can't sum vectors of different sizes.\n";
        return;
    }
    for (int i = 0; i < a.size(); i++) {
        a[i] = a[i] + b[i];
    }
    return;
}

//scalar product between vectors;
//@a: first vector, const;
//@b: second vector, const;
//return: double scalar product a*b;
double dotProduct(const std::vector<double> &a, const std::vector<double> &b) {
    if (a.size() != b.size()) {
        std::cerr << "Error in dotProduct: can't multiply vectors of different sizes.\n";
        return -1.;
    }
    double s = 0;
    for (int i = 0; i < a.size(); i++)
        s += a[i] * b[i];
    return s;
}

//product of a vector by a scalar;
//@s: double, scalar value;
//@a: std::vector, the vector to be multiplied;
//return: std::vector s*a;
std::vector<double> productByScalar(double s, const std::vector<double> &a) {
    auto temp = a;
    for (int i = 0; i < a.size(); i++)
        temp[i] = s * a[i];
    return temp;
}

//Subgradient of the Hinge loss function;
//let z = y*(t(w)*x) be the product of the expected output and the real output;
//if z<=1, the value of the subgradient of the Hinge loss function is -1;
//if z>1, the value of the subgradient of the Hinge loss function is 0;
//@y: label of the training point x;
//@wt: vector on which the GD is acting;
//@x: training point;
//return: int, {-1,0};
int dl(int y, const std::vector<double> &w, const std::vector<double> &x) {
    if (y * dotProduct(w, x) <= 1)
        return -1;
    else
        return 0;
}

//vector in the subgradient of the E.R.;
//@y: vector of outputs (i-th element of y contains the classification (-1 or 1) of the i-th training point);
//@x: matrix of data (i-th row of x contains the coordinates of the i-th training point);
//@lamb: regularization parameter
//@wt: vector on which the GD is acting
std::vector<double> vt(const std::vector<int> &y, const std::vector<std::vector<double>> &x, const double lamb, const std::vector<double> &wt) {
    int n = wt.size(), xSize = x.size();
    std::vector<double> v(n, 0.);
    for (int i = 0; i < xSize; i++) {
        double s = (y[i] * dl(y[i], wt, x[i])) / (double)xSize;
        auto temp = productByScalar(s, x[i]);
        sumVectorsByComponents(v, temp);
    }
    sumVectorsByComponents(v, productByScalar(2 * lamb, wt));
    return v;
}

//vector in the subgradient of the E.R. in kernel formulation;
//@y: vector of outputs (i-th element of y contains the classification (-1 or 1) of the i-th training point);
//@x: matrix of data (i-th row of x contains the coordinates of the i-th training point);
//@wt: vector on which the GD is acting
//@k: kernel map
std::vector<double> S(const std::vector<int> &y, const std::vector<std::vector<double>> &x, const std::vector<double> &wt, const std::function<double(std::vector<double>, std::vector<double>)> &k) {
    int xSize = x.size();
    std::vector<double> s(xSize, 0.);
    for (int i = 0; i < xSize; i++) {
        if (y[i] * k(x[i], wt) <= 1) {
            s[i] = -y[i];
            s[i] = s[i] / xSize;
        }
    }
    return s;
}

//train method
//Finds the minimizator of the E.R. using GD
//@x: matrix of data (i-th row of x contains the coordinates of the i-th training point);
//@y: vector of outputs (i-th element of y contains the classification (-1 or 1) of the i-th training point);
//@tStop: number of iterations in Gradient Descend algorithm;
//@lamb: regularization parameter;
void svm::train(const std::vector<std::vector<double>> &x, const std::vector<int> &y, const int tStop, const double lambda) {
    if (x.size() != y.size()) {
        std::cerr << "Error in SVM train: x and y must have the same size;\n";
        return;
    }

    if (m_type != "kernel") {
        //random initialization of wt
        std::default_random_engine g;
        //if the seed hasn't been set, set seed
        if (m_seed == 0)
            g.seed(time(NULL));
        for (int i = 0; i < m_n; i++) {
            m_wt[i] = (double)g() / (double)g.max();
        }

        //if in feature case, first evaluate the mapped training points and then minimize ER
        if (m_type == "feature") {
            //mapped training points
            std::vector<std::vector<double>> xFeature;
            xFeature.resize(x.size());
            for (int i = 0; i < x.size(); i++) {
                xFeature[i] = m_featureMapFun(x[i]);
            }
            //Maximum squared norm of xFeature
            double B = maxSquaredNorm(xFeature) + 2 * lambda;
            B = 1 / B;
            //GD
            for (int t = 1; t <= tStop; t++) {
                auto temp = productByScalar(-std::sqrt(B / (double)t), vt(y, xFeature, lambda, m_wt));
                sumVectorsByComponents(m_wt, temp);
                if (m_verbose and t % (tStop/100) == 0) {
                    std::cout << "Training... " << ((double)t / (double)tStop) * 100 << "%\r";
                }
            }
        }

        //if in linear case, minimize the ER
        else if (m_type == "linear") {
            //Maximum squared norm of x
            double B = maxSquaredNorm(x) + 2 * lambda;
            B = 1 / B;
            //GD
            for (int t = 1; t <= tStop; t++) {
                auto temp = productByScalar(-std::sqrt(B / (double)t), vt(y, x, lambda, m_wt));
                sumVectorsByComponents(m_wt, temp);
                if (m_verbose and t % (tStop/100) == 0) {
                    std::cout << "Training... " << ((double)t / (double)tStop) * 100 << "%\r";
                }
            }
        }
    }
    //if in kernel case
    else if (m_type == "kernel") {
        int xSize = x.size();
        m_wt.resize(xSize);
        //random initialization of wt
        std::default_random_engine g;
        //if the seed hasn't been set, set seed
        if (m_seed == 0)
            g.seed(time(NULL));
        for (int i = 0; i < xSize; i++) {
            m_wt[i] = (double)g() / (double)g.max();
        }
        //Maximum squared norm of x
        double B = maxSquaredNorm(x) + 2 * lambda;
        B = 1 / B;
        //G.D.
        for (int t = 1; t <= tStop; t++) {
            auto c = S(y, x, m_wt, m_kernel);
            sumVectorsByComponents(c, productByScalar(2 * lambda, m_wt));
            c = productByScalar(-std::sqrt(B / (double)t), c);
            sumVectorsByComponents(m_wt, c);
            if (m_verbose and t % (tStop/100) == 0) {
                std::cout << "Training... " << ((double)t / (double)tStop) * 100 << "%\r";
            }
        }
    }

    else {
        std::cerr << "Error in training: undefined type.\n";
    }
    return;
}

//Get the classification of a point:
//@x: vector of the input space that is to be classified;
//return: classification of @x based on the best minimizer of ER found in training;
//return type: int {-1,1};
int svm::classify(const std::vector<double> &x) const {
    double pred;
    //if feature map, first evaluate the mapped point and then classify
    if (m_type == "feature")
        pred = dotProduct(m_wt, m_featureMapFun(x));
    //if linear case, classify the point
    else if (m_type == "linear")
        pred = dotProduct(m_wt, x);
    //if in kernel case, evaluate the kernel in x
    else if (m_type == "kernel") {
        pred = m_kernel(x, m_wt);
    }

    else {
        std::cerr << "Error in classify: undefined type.\n";
    }
    //return the sign of the prediction
    return (pred > 0) - (pred < 0);
}

//Get the classification of a point with variable SVM treshold:
//@x: vector of the input space that is to be classified;
//@treshold: the value of the threshold between signal and noise;
//return: classification of @x based on the best minimizer of ER found in training;
//return type: int {-1,1};
int svm::classify(const std::vector<double> &x, double threshold) const {
    double pred;
    //if feature map, first evaluate the mapped point and then classify
    if (m_type == "feature")
        pred = dotProduct(m_wt, m_featureMapFun(x));
    //if linear case, classify the point
    else if (m_type == "linear")
        pred = dotProduct(m_wt, x);
    //if in kernel case, evaluate the kernel in x
    else if (m_type == "kernel") {
        pred = m_kernel(x, m_wt);
    }

    else {
        std::cerr << "Error in classify: undefined type.\n";
    }
    //return the sign of the prediction
    return (pred>threshold) ? 1 : -1;
}

//Get the regression of a point:
//@x: vector of the input space that is to be classified;
//return: classification of @x based on the best minimizer of ER found in training;
//return type: double;
double svm::regression(const std::vector<double> &x) const {
    double pred;
    //if feature map, first evaluate the mapped point and then classify
    if (m_type == "feature")
        pred = dotProduct(m_wt, m_featureMapFun(x));
    //if linear case, classify the point
    else if (m_type == "linear")
        pred = dotProduct(m_wt, x);
    //if in kernel case, evaluate the kernel in x
    else if (m_type == "kernel") {
        pred = m_kernel(x, m_wt);
    }

    else {
        std::cerr << "Error in classify: undefined type.\n";
    }
    //return the sign of the prediction
    return pred;
}