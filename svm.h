//*****************************//
//Class svm: implements a support vector machine for classification purpose;
//Algorithm: ERM of the Hinge loss function by gradient descend;
//Space of inputs: R^d (eventually declare a feature map or a kernel);
//Space of outputs: {-1,1};
//It can be used both in linear case and in non linear case (by defining an external feature map or kernel);
//Inheritance: None;
//Author: Derin Lucio;
//*****************************//

//*******CONSTRUCTORS:********//
//Linear case:
//@n: int, required, dimension of the space of inputs (i.e. the number of features);
//@type: string that specifies the type, optional: default: linear.
//@verbose: optional, bool to enable verbose output (such as %process while training), default: false;
//svm(int n, string type, bool verbose)

//Feature case:
//@n: int, required, dimension of the space of features (i.e. the dimension of the elements in the image of the feature map);
//@type: string that specifies the type, required: value must be set to "feature".
//@featureMap: optional, the external feature map; default: nullptr;
//@verbose: optional, bool to enable verbose output (such as %process while training), default: true;
//svm(int n, string type, const std::function<std::vector<double>(std::vector<double>)> &map, bool verbose);

//Kernel case:
//@n: int, required, dimension of the space of inputs (i.e. the number of features);
//@type: string that specifies the type, required: value must be set to "kernel".
//@ker: optional, the external kernel map; default: nullptr;
//@verbose: optional, bool to enable verbose output (such as %process while training), default: true;
//svm(int n, const std::function<double(std::vector<double>,std::vector<double>)> &ker = nullptr, bool verbose = false);

//**********SETTERS:**********//
//void specifyType(const std::string) -> specify the type of algorithm ("linear","feature","kernel")
//void enableVerbosePrinting() -> Enable verbose printing;
//void disableVerbosePrinting() -> Disable verbose printing;
//void setInputSpaceDim(int n) -> Set input space dimension (number of features) equal to @n; clears m_wt and preallocates space
//void setSeed(double seed) -> Set seed for the random initialization of wt;
//void setFeatureMap(const std::function<std::vector<double>(std::vector<double>)>) -> Set feature map;
//void setKernel(const std::function<double(std::vector<double>, std::vector<double>)>) -> Set kernel map;

//**********GETTERS:**********//
//int getInputSpaceDim() const -> Get the dimension of the input space (number of features);
//std::vector<double> getWt() const -> get the current wt;

//**********METHODS:**********//
//Train the svm:
//@x: matrix of data (i-th row of x contains the coordinates of the i-th training point);
//@y: vector of outputs (i-th element of y contains the classification (-1 or 1) of the i-th training point);
//@tStop: number of iterations in Gradient Descend algorithm;
//@lamb: regularization parameter;
//wt is initialized as a vector of random number;
//The training process implements the gradient descend and finds Wt_hat, the minimizer of the empirical risk;
//At the end of the training process, m_wt (private member of the class) is the best minimizer found;
//void train(const std::vector<std::vector<double>> &x, const std::vector<int> &y, const int tStop, const double lamb);

//Get the classification of a point:
//@x: vector of the input space that is to be classified;
//return: classification of @x based on the best minimizer of ER found in training;
//return type: int {-1,1};
//int classify(const std::vector<double> &x) const;

//****************************//

//Notoation:
//ERM = empirical risk minimization;
//ER = empirical risk;
//GD = Gradient Descend;
//wt = vector updated in the gradient descend;
//wt_hat = best minimizer of ER found with GD;

//feature map = function that maps input parameters in a vector space on which the SVM can work;
//fMap: R^d -> R^d; x |-> fMap(x);

//kernel map: kernel evaluation of the regression in the point x;
//kerMap: R^d x R^d -> R; x |-> sum(K(x,xTrainingj),wtj);
//where:
//x is the point of evaluation of the regression;
//xTrainingj is the j-th element of the training set;
//wtj is the j-th element of the vector on which the GD algorithm is acting;
//****************************//

#ifndef _SVM
#define _SVM

#include <array>
#include <functional>
#include <string>
#include <vector>
class svm {
  private:
    //input space dimension
    int m_n;
    //seed for random wt initialization
    double m_seed = 0;
    //wt
    std::vector<double> m_wt;
    //verbose handler boolean
    bool m_verbose;
    //string that specify the type (linear, feature map, kernel)
    //possible values:
    //"linear"
    //"feature"
    //"kernel"
    std::string m_type;
    //feature map
    std::function<std::vector<double>(const std::vector<double>)> m_featureMapFun;
    //kernel map
    std::function<double(std::vector<double>, std::vector<double>)> m_kernel;

  public:
    //Constructor:
    //Linear
    svm(int n, std::string type = "linear", bool verbose = false) : m_n(n), m_verbose(verbose), m_type(type) {
        m_wt.resize(m_n);
        m_type = "linear";
    }
    //Feature
    svm(int n, std::string type, const std::function<std::vector<double>(std::vector<double>)> &map = nullptr, bool verbose = false) : m_n(n), m_verbose(verbose), m_featureMapFun(map) {
        m_wt.resize(m_n);
        m_type = "feature";
    }
    //Kernel
    svm(int n, std::string type, const std::function<double(std::vector<double>, std::vector<double>)> &ker = nullptr, bool verbose = false) : m_n(n), m_verbose(verbose), m_kernel(ker) { m_type = "kernel"; }

    //Methods:
    void train(const std::vector<std::vector<double>> &x, const std::vector<int> &y, const int tStop, const double lamb);
    int classify(const std::vector<double> &x) const;
    int classify(const std::vector<double> &x, double treshold) const;
    double regression(const std::vector<double> &x) const;

    //Setters:
    void specifyType(const std::string &type) { m_type = type; }
    void enableVerbosePrinting() { m_verbose = true; }
    void disableVerbosePrinting() { m_verbose = false; }
    void setInputSpaceDim(int n) {
        m_n = n;
        m_wt.clear();
        m_wt.resize(n);
    }
    void setSeed(double seed) { m_seed = seed; }
    void setKernel(const std::function<double(std::vector<double>, std::vector<double>)> &ker) { m_kernel = ker; }
    void setFeatureMap(const std::function<std::vector<double>(std::vector<double>)> &map) { m_featureMapFun = map; }

    //Getters:
    int getInputSpaceDim() const { return m_n; }
    std::vector<double> getWt() const { return m_wt; }
};

#endif //_SVM