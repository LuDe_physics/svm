## Python and C++ classes for SVM classification
Implementation of a SVM based on the minimization, with Gradient Descent algorithm, of the strongly convex expected risk built on the Hinge Loss:\
$`\hat{L}_{\lambda} = \frac{1}{N}\sum_{i=1}^{N} |1-y_i(\overrightarrow{x}_i \cdot \overrightarrow{w})|_{+} + \lambda||w||^2`$

where:
 - $`\overrightarrow{x}_i`$ is the i-th training point;
 - $`y_i`$ is the i-th label that classifies the point i (possible values are: +1 or -1);
 - $`\overrightarrow{w}`$ is the variable;
 - $`N`$ is the numerosity of the training set;
 - $`\lambda`$ is the regularization parameter;

The minimizer $`w^{*}`$ which solves $`\min_{w \in \mathbb{R}^d} \hat{L}_{\lambda}`$ (where d is the dimension of the input space) parametrizes the hyperplane that best separates the data.
The SVM can be used in linear case, or with a feature or kernel map (implemented externally and passed by const reference in the constructor).
A feature map is a map from $`\mathbb{R}^d`$ to $`\mathbb{R}^p`$, that maps the training points from the input space in a feature space of dimension $`p`$; it must be implemented as a function that returns a vector and takes a vector as parameter.
A kernel map is defined as:\
$`f(x) = \sum_{i = 1}^{N} k(x,x_i)(w)_i`$, where $`k(x_i,x_j)`$ is a semi-positive defined kernel; it must be implemented as a function that returns a double and takes two vectors as parameters.

### C++ version:
Usage:
 - Instantiate an object of the class;
 - Train the SVM;
 - Get the classification of other points with the corresponding method;
 - Have a look at the source code, at the comments, and at the given examples;

### Python version:
Usage:
 - Instantiate an object of the class;
 - Train the SVM;
 - Get the classification of other points with the corresponding method;
 - Have a look at the source code, at the comments, and at the given examples;

 ### Upcoming feature:
    [ ] Nystrom method for the reduction of the dimension of the training set;
    [ ] Accelerated Gradient Descent Algorithm (Nesterov) for a faster training on big dataset;
    [ ] Stochastic Gradient Descent for a very fast but inaccurate training on big dataset (useful for on line training);
    [ ] Soft thresholding algorithm (ISTA and FISTA) for training on sparse signals;

#### Notes:
 - C++ examples use ROOT program, developed by CERN;
 - This is an amateur project: it's never been cross-checked or heavily tested, so there might be some errors!
 - **Feel free to report bug or suggestions!**